<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
use App\Models\Course;

class StudentController extends Controller
{


    public function index()
    {
        $students=Student::all();
        return view ('students',['students'=>$students]);

    }

    public function create()
    {
        $courses=Course::all();
        return view('student-registration',[
            'courses'=>$courses
        ]);
    }

    public function store(Request $request)
    {
        $student = new Student();
        $student->first_name = $request->first_name;
        $student->last_name = $request->last_name;
        $student->email = $request->email;
        $student->date_of_birth = $request->date_of_birth;
        $student->course_id = $request->course_id ?? 1; // Use default value of 1 if course_id is not provided
        $student->save();
        return redirect('students');
    }

}

