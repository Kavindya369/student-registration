<!DOCTYPE html>
<html>

<head>
    <title>Add Items to Cart</title>
</head>

<body>
<h1>Add Items to Cart</h1>

<div>
    <form action="" method="post">
        <label for="items_id">Item:</label><br>
        <select name="items_id" id="items_id">
            @foreach($items as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
        </select><br><br>

        <label for="quantity">Quantity:</label><br>
        <input type="text" name="quantity" id="quantity"><br><br>

        <button type="submit">Add to Cart</button>
    </form>
</div>

<table border="1">
    <tr>
        <th>ID</th>
        <th>Item</th>
        <th>Price</th>
        <th>Quantity</th>
        <th>Total</th>
    </tr>
    @foreach($carts as $cart)
        <tr>
            <td>{{ $cart['id'] }}</td>
            <td>{{ $cart['name'] }}</td>
            <td>{{ $cart['price'] }}</td>
            <td>{{ $cart['quantity'] }}</td>
            <td>{{ $cart['total'] }}</td>
        </tr>
    @endforeach
</table>
</body>

</html>
