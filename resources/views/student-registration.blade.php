<!DOCTYPE html>
<html lang="en">
<head>
    <title>Registration Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        /* Style the header */
        header {
            background-color: #333;
            color: white;
            padding: 10px;
            text-align: center;
        }

        /* Style the form container */
        .container {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding: 20px;
            margin: 50px auto;
            background-color: #f2f2f2;
            border-radius: 5px;
            box-shadow: 0 0 10px rgba(0,0,0,0.2);
            max-width: 600px;
        }

        /* Style the form inputs */
        input[type=text], input[type=email], select, input[type=date] {
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
            font-size: 16px;
        }

        /* Style the form labels */
        label {
            display: inline-block;
            margin-bottom: 6px;
            font-size: 18px;
            font-weight: bold;
        }

        /* Style the form submit button */
        input[type=submit] {
            background-color: #4CAF50;
            color: white;
            padding: 12px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 18px;
            margin-top: 20px;
        }

        /* Style the form submit button on hover */
        input[type=submit]:hover {
            background-color: #45a049;
        }

        /* Style the table */
        table {
            border-collapse: collapse;
            width: 100%;
            margin-top: 50px;
        }
        th, td {
            text-align: left;
            padding: 8px;
            border-bottom: 1px solid #ddd;
        }
        th {
            background-color: #4CAF50;
            color: white;
        }

        /* Style the view data button */
        .button {
            display: inline-block;
            border-radius: 4px;
            background-color: #4CAF50;
            color: white;
            text-align: center;
            padding: 12px;
            margin-top: 50px;
            text-decoration: none;
            cursor: pointer;
        }

        /* Style the view data button on hover */
        .button:hover {
            background-color: #45a049;
        }
    </style>
</head>


<body>
<h1>Registration Form</h1>
<form method="post" action="">
    @csrf

    <label for="first_name">First Name:</label>
    <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" >
    @error('first_name')
    <div style="color: red">{{ $message }}</div>
    @enderror

    <label for="last_name">Last Name:</label>
    <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" >
    @error('last_name')
    <div style="color: red">{{ $message }}</div>
    @enderror

    <label for="email">Email:</label>
    <input type="email" name="email" id="email" value="{{ old('email') }}" >
    @error('email')
    <div style="color: red">{{ $message }}</div>
    @enderror

    <label for="date_of_birth">Date of Birth:</label>
    <input type="date" name="date_of_birth" id="date_of_birth" value="{{ old('date_of_birth') }}" >
    @error('date_of_birth')
    <div style="color: red">{{ $message }}</div>
    @enderror

    <label for="course_id" class="block">Course Enrolled</label>
    <select name="course_id" id="course_id" class="border border-gray-300 rounded p-2 w-full">
        @foreach($courses as $course)
            <option value="{{ $course->id }}" {{ old('course_id') == $course->id ? 'selected' : '' }}>
                {{ $course->name }}
            </option>
        @endforeach
    </select>
    @error('course_id')
    <div style="color: red">{{ $message }}</div>
    @enderror

    <input type="submit" value="Register">
</form>
</body>

</html>




