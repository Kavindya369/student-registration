<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('items')->insert([
            [
                'name' => 'Apple',
                'price' => '120',
            ],
            [
                'name' => 'Orange',
                'price' => '150',
            ],
            [
                'name' => 'Mango',
                'price' => '130',
            ],

        ]);
    }
}

